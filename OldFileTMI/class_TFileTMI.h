#ifndef class_TFileTMIH
#define class_TFileTMIH
//---------------------------------------------------------------------------
#include <class_ParameterTMI.h>
#include <class_Errors.h>
#include <vcl.h>
//---------------------------------------------------------------------------
/*
����� TFileTMI ������������ ��� ������ � ���������� ������ ������ ����������.
��������� ������ � ��������� ������� �� �����������.
��� ������ ����� ����������� ��������� �������� ������.

������� LoadFileTMI ��������� ���� � ��������� �� ������������ ������ ���������� � �����.
� ������, ���� �����-�� �� ���������� ������� ����� ������� ��� ������� ���� �������� ������������ ���������
��������������� ��������� � ���������� �������� � ���� ������ ������������.

������� GetParameter ����������� � ���� ��������� ������ �������������� ��������� � ���������� ������ ����
TParameterTMI. ��� ������ ������� TParameterTMI - <double, double>.
� ������, ���� �������� ������� ����� ������� ��� ������� ���� �������� ������������ ������� ���������� 0.
*/
//---------------------------------------------------------------------------
class TFileTMI
{
    private:
        TErrors Errors;
        unsigned int CountPar;      //���������� ���������� (��� ����� �������)
        unsigned int TimeBytes;     //���������� ���� ������� (4 ��� 5 ����)
        unsigned int ZeroCode;      //��� 0 �
        unsigned int STCode;        //��� 6.2 �
        unsigned int CountPoint;    //���������� ������� (�����) � �����
        char FileType;              //��� ����� ('f' - ��������������, 's' - ����������, 'd' - ��������)
        unsigned int BuffLength;    //����� �������� ��� ������
        TFileStream *File;
        double GetTime(unsigned int TimeBytes, unsigned __int8 *Mas);           //����������� ����� ������� (� ��������)
        double GetSigPar(unsigned int ParameterIndex, unsigned __int8 *Mas);    //������������ ��������� ��� ���������� ������
        TPointTMI<double, double> GetPoint(unsigned int ParameterIndex,         //��������� �����
                                                unsigned __int8 *Mas);          
        bool TimeCheck(void);                                                   //�������� ����� ������� ������� �� ������������

    public:
        TFileTMI();
        ~TFileTMI();
        int GetError(AnsiString &Message);                                      //���������� �� ������, ����� ������� ��������� �� ��������
        int GetWarning(AnsiString &Message);                                    //��������� � �� �������� ������, ��������� ����������

        //�������� ����� � �������� ��� �� ������� ������
        //��������� �������:
            //FileName ������ ��� ����
            //FileType 'f' - ��������������, 's' - ���������� 'd' - ��������
            //CountPar ���������� ���������� � �����
            //TimeBytes ����������� ���� ��������� �� ����� (4 ��� 5 ����)
            //BuffLength ����� ����� ������ ����� - �������� ����� ������ ��������.
            //����� ����� �� ������ ���� ������ ����� ����� ������ � �����.
        bool LoadFileTMI(AnsiString FileName, char FileType, unsigned int CountPar,
                                unsigned int TimeBytes, unsigned int BuffLength);

        //��������� ���������
        //�������� �������:
            //ParameterIndex ������ �������������� ���������
        TParameterTMI<double, double>* GetParameter(unsigned int ParameterIndex);

};
//---------------------------------------------------------------------------
TFileTMI::TFileTMI()
{
    CountPar = 0;
    TimeBytes = 0;
    ZeroCode = 0;
    STCode = 0;
    CountPoint = 0;
    TimeBytes = 0;
    FileType = 0;
    BuffLength = 0;
    File = NULL;

    Errors.SetErrorMessage(1, "���� �� ������.");
    Errors.SetErrorMessage(2, "���������� ���������� ������ �������.");
    Errors.SetErrorMessage(3, "���������� ���� ������� ������ �������.");
    Errors.SetErrorMessage(4, "���� ����");

    Errors.SetErrorMessage(5, "���� �� �������� �� ����� ����� ������, ��������������� �������� ����������.");
    Errors.SetErrorMessage(6, "��������� ��� ������ � ��������� ����������� ����� ����������� �����.");
    Errors.SetErrorMessage(7, "���� �� ������.");
    Errors.SetErrorMessage(8, "��� ����� ����� �� �����.");

    Errors.SetErrorMessage(9, "����� �� ���������.");
    Errors.SetErrorMessage(10, "������ ������ ��� ������ ����� �������.");
    Errors.SetErrorMessage(11, "������ ���.");

    Errors.SetErrorMessage(12, "�� ����� ������ ����� ���������.");
    Errors.SetWarningMessage(1, "���� �� �������� ������ ����� �������. �������� �� ��������� ��� ��������� ������ �������.");
    Errors.SetWarningMessage(2, "������ ���.");

}
//---------------------------------------------------------------------------
int TFileTMI::GetError(AnsiString &Message)
{
    return Errors.GetError(Message);
}

int TFileTMI::GetWarning(AnsiString &Message)
{
    return Errors.GetWarning(Message);
}
//---------------------------------------------------------------------------
TFileTMI::~TFileTMI()
{
    delete  File;
}
//---------------------------------------------------------------------------
bool TFileTMI::LoadFileTMI(AnsiString FileName, char FileType, unsigned int CountPar,
                            unsigned int TimeBytes, unsigned int BuffLength)
{

    //��������
    if(!FileExists(FileName))
    {
        Errors.ClearError();
        Errors.SetError(1,"");
        return false;
    }

    this->CountPar = CountPar;
    this->TimeBytes = TimeBytes;
    this->FileType = FileType;


    if(TimeBytes !=4 && TimeBytes != 5)
    {
        Errors.ClearError();
        Errors.SetError(3,"");
        return false;
    }

    if (BuffLength<CountPar)
    {
        Errors.ClearError();
        Errors.SetError(10,"");
        return false;
    }
    this->BuffLength = BuffLength;

    delete  File;
    File = new TFileStream(FileName, fmOpenRead);

    //�������� ����� �� ������� � ��� �����-���� ������
    if(File->Size == 0)
    {
        Errors.ClearError();
        Errors.SetError(4,"");
        return false;
    }

    //�������� �� ���� ������ ���� ����� ������
    if(File->Size < (CountPar + TimeBytes) != 0)
    {
        Errors.ClearError();
        Errors.SetError(5,"");
        return false;
    }

    //��� �����
    if(FileType == 'd')
    {
        Errors.ClearError();
        Errors.SetError(6,"");
        return false;
    }

    //if(FileType != 'f' && FileType != 's' && FileType != 'd')
    if(FileType != 'f' && FileType != 's')
    {
        Errors.ClearError();
        Errors.SetError(8,"");
        return false;
    }

    if(FileType == 'f' && CountPar == 0)
    {
        Errors.ClearError();
        Errors.SetError(2,"");
        return false;
    }

    // ���������� ���������� � ���������� ����� ������ ������ ������
    if(FileType == 's' && ((CountPar % 8) != 0))
    {
        Errors.ClearError();
        Errors.SetError(2,"");
        return false;
    }

    // ��� ��������� ������ ������ ��������� � CountPar ��� � ������� ����� ������
    if (FileType=='f')
        this->CountPar = CountPar + TimeBytes;

    if (FileType == 's')
        this->CountPar = CountPar / 8 + TimeBytes;

    if(!TimeCheck())
        return false;

    return true;
}
//---------------------------------------------------------------------------
bool TFileTMI::TimeCheck(void)
{
    /*��������� ������� TimeCheck ��������� ����� � ����� �� ������������,
    �, � ������ ������� �����, ��������� ��������������� ���������,
    � ������� ������� ������� ������� �����.*/

    //�������� �� ���� ����� ����� �������. ���� �� ����� �����, �� ���������� �������������� � ���������� ��������
    if(File->Size % CountPar != 0)
    {
        Errors.ClearWarning();
        Errors.SetWarning(1,"");
    }

    //���������� ������� (�����)
    CountPoint = File->Size / CountPar;

    //������ �������-������ ��� ������ �� ����� (� ������)
    unsigned int Bytes = (unsigned int)BuffLength / CountPar * CountPar * 1024;

    //�������� ������� �� ������������. ���� ���� �� ���������, �� ������� ������������ ����� � ���������, �� ������� ����� �� ��������� � ������� �� ���������.
    AnsiString ErrLine = "\n";
    double Time1 = -1;
    double Time2 = 0;
    unsigned int FailurePointsCounter = 0;
    unsigned int k = 0;
    unsigned __int8 *Mas;

    // ���� ������ �����
    Mas = new __int8[Bytes];

    File->Position = 0;
    for (unsigned int i = 0; i < CountPoint * CountPar / Bytes; i++)
    {
        File->Read(Mas, Bytes);
        for (unsigned int j = 0; j < Bytes; j = j + CountPar)
        {
            Time2 = GetTime(TimeBytes, &Mas[j]);
            if (Time1 > Time2)
            {
                ErrLine = ErrLine + "��� ����� " + (AnsiString)(k - 1) + " � " + (AnsiString)k + "\n";
                FailurePointsCounter++;
                if(FailurePointsCounter > 10)
                {
                    ErrLine = " ������� ����� ������� ����� (������ ������).";
                    //break;
                    j = Bytes;
                    i = CountPoint * CountPar / Bytes;
                }
            }
            Time1 = Time2;
            k++;
        }
    }

    if(FailurePointsCounter < 10)
        if ((CountPoint * CountPar) % Bytes != 0)
        {
            Bytes = (CountPoint * CountPar) % Bytes;
            File->Read(Mas, Bytes);
            for (unsigned int j = 0; j < Bytes; j = j + CountPar)
            {
                Time2 = GetTime(TimeBytes, &Mas[j]);
                if (Time1 > Time2)
                {
                    ErrLine = ErrLine + "��� ����� " + (AnsiString)(k - 1) + " � " + (AnsiString)k + "\n";
                    FailurePointsCounter++;
                    if(FailurePointsCounter > 10)
                    {
                        ErrLine = " ������� ����� ������� ����� (������ ������).";
                        break;
                    }
                }
                Time1 = Time2;
                k++;
            }
        }

    if (ErrLine != "\n")
    {
        Errors.ClearError();
        Errors.SetError(9, ErrLine);
        delete [] Mas;
        return false;
    }

    delete [] Mas;
    return true;
}
//---------------------------------------------------------------------------
TParameterTMI<double, double>* TFileTMI::GetParameter(unsigned int ParameterIndex)
{
    if(File == NULL)     //���� ���� �� ��������, �� ������
    {
        Errors.ClearError();
        Errors.SetError(7,"");
        return 0;
    }
    unsigned int CountParFile=0;

    switch(FileType)
    {
        case 'f':   CountParFile= CountPar- TimeBytes;      //������� � ���������� ������������� ����������� ����������
                    break;

        case 's':   CountParFile=(CountPar - TimeBytes)*8;  //������� � ���������� ������������� ����������� ����������
                    break;

        //case 'd':   ShowMessage("d");
                    //break;
    }  

    if(ParameterIndex<1 || ParameterIndex>CountParFile)
    {
        Errors.ClearError();
        Errors.SetError(12,"");
        return 0;
    }

    TParameterTMI<double, double> *Parameter = new TParameterTMI<double, double>;
    Parameter->CountPoint = CountPoint;

    unsigned int Bytes = (unsigned int)BuffLength / CountPar * CountPar * 1024;
    unsigned __int8 *Mas;

    Mas = new __int8[Bytes];

    // �������� ����������� ��������� �� �������, ��
    File->Position = 0;
    unsigned int k = 0;
    for (unsigned int i = 0; i < CountPoint * CountPar / Bytes; i++)
    {
        File->Read(Mas, Bytes);
        for (unsigned int j = 0; j < Bytes; j = j + CountPar)
        {
            Parameter->Points[k] = GetPoint(ParameterIndex, &Mas[j]);
            k++;
        }
    }
    if ((CountPoint * CountPar) % Bytes != 0)
    {
        Bytes = (CountPoint * CountPar) % Bytes;
        File->Read(Mas, Bytes);
        for (unsigned int j = 0; j < Bytes; j = j + CountPar)
        {
            Parameter->Points[k] = GetPoint(ParameterIndex, &Mas[j]);
            k++;
        }
    }

    delete [] Mas;
    return Parameter;
}
//---------------------------------------------------------------------------
TPointTMI<double, double> TFileTMI::GetPoint(unsigned int ParameterIndex, unsigned __int8 *Mas)
{
    /*��������� ������� GetPoint �������� �� ���� ������ ���������� ��������,
    ��������� �� �����-������ � ����������� ������ ������ TPointTMI.
    ������� ��������� ��������� "�����", � ������� � ������� <double, double>
    �������� �������� ������� � �������� �������������� ���������.*/

    TPointTMI<double,double> Point;

    Point.X = GetTime(TimeBytes, Mas);

    //� ����������� �� ���� �����, ����� ����� �������� ��������� ������������ ��������
    switch(FileType)
    {
        case 'f':   Point.Y = (double)Mas[TimeBytes + ParameterIndex - 1];
                    break;

        case 's':   Point.Y = GetSigPar(ParameterIndex, Mas);
                    break;

        //case 'd':   ShowMesage("d");
                    //break;
    }

    return Point;
}
//---------------------------------------------------------------------------
double TFileTMI::GetTime(unsigned int TimeBytes, unsigned __int8 *Mas)
{
    /*��������� ������� GetTime � �������� ���������� ������� ���������� ���� ������
    � ��������� �� �����-������ � ������� ����������� ������� �����.
    ������� GetTime ��������� �����, ��� ������ ��������������� ������� - double.*/

    //�������� ������� � �
    double Time;
    unsigned int  Time_Sec, Time_MilSec;

    if (TimeBytes == 4)
    {
        //�������
        Time_Sec = (unsigned int)Mas[0];
        Time_Sec = (Mas[1] >> 4) | (Time_Sec << 4);

        //������������
        Time_MilSec = (((unsigned int)Mas[1] << 16) & 0x000F0000) | ((unsigned int)Mas[2] << 8) | (unsigned int)Mas[3];

        //�����
        Time = (double)Time_Sec + (double)Time_MilSec / 1000000;
    }

    if (TimeBytes == 5)
    {
        Time_Sec = (unsigned int)Mas[2];
        Time_Sec = (Time_Sec>>4) | (Mas[1] << 4)|(Mas[0] << 12);
        Time_MilSec = ((unsigned int)Mas[2] << 16 & 0x000F0000) + ((unsigned int)Mas[3] << 8) + (unsigned int)Mas[4];
        Time = (double)Time_Sec + (double)Time_MilSec / 1000000;
    }

    return Time;
}
//---------------------------------------------------------------------------
double TFileTMI::GetSigPar(unsigned int ParameterIndex, unsigned __int8 *Mas)
{
    /*��������� ������� GetSigPar ������������ ��� ������������ ����������� ��������� �� �������������� �������.
    ����������� ������� GetSigPar �������� ������ ���������� �������� � ��������� �� �����-������.*/

    //����� ������������� ��������� ��� ���������� ������
    struct TMyByte
    {
        unsigned __int8 Bit1:1;
        unsigned __int8 Bit2:1;
        unsigned __int8 Bit3:1;
        unsigned __int8 Bit4:1;
        unsigned __int8 Bit5:1;
        unsigned __int8 Bit6:1;
        unsigned __int8 Bit7:1;
        unsigned __int8 Bit8:1;
    } Mybyte;

    unsigned int Par = 0;
    unsigned int ParByteIndex = ParameterIndex / 8;         //������ �����, �������������� ���������, � ������
    TMyByte* pMB = NULL;
    pMB = (TMyByte*)(Mas + TimeBytes + ParByteIndex);
    unsigned int ParBitIndex = (ParameterIndex % 8) - 1;    //������ ����, �������������� ���������, � �����

    switch(ParBitIndex)
    {
        case 0:     Par = pMB->Bit8;
                    break;

        case 1:     Par = pMB->Bit7;
                    break;

        case 2:     Par = pMB->Bit6;
                    break;

        case 3:     Par = pMB->Bit5;
                    break;

        case 4:     Par = pMB->Bit4;
                    break;

        case 5:     Par = pMB->Bit3;
                    break;

        case 6:     Par = pMB->Bit2;
                    break;

        case 7:     Par = pMB->Bit1;
                    break;
    }

    return Par;
}
//---------------------------------------------------------------------------
#endif
