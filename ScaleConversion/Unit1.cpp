//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"

#include "class_FileTP.h"
#include "scaleconv.h"
#include "somefunctions.h"
#include "class_Errors.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
    : TForm(Owner)
{
    AnsiString CFGFolderName =  ExtractFilePath(Application->ExeName) + "tsucfgs\\";
    CFGFileListBox->Directory = CFGFolderName;
    CFGFileListBox->Mask = "*.tsucfg";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ScaleConv_BTNClick(TObject *Sender)
{
    AnsiString InputFileName = InputFileListBox->FileName;
    AnsiString CFGFileName = CFGFileListBox->FileName;
    unsigned int InputFileCountPar;
    unsigned int InputFileParNum;
    if(!TryStrToInt(LE_CountPar->Text, InputFileCountPar) || !TryStrToInt(LE_ParNum->Text, InputFileParNum))
    {
        Application->MessageBox("��������� ��������� ��� ���� �����.", "��������!", MB_OK | MB_ICONWARNING);
        return;
    }
    InputFileCountPar = LE_CountPar->Text.ToInt();
    InputFileParNum = LE_ParNum->Text.ToInt();
    char InputFileType = CB_InputFileType->Text[1];
    char InputFileDataType = CB_InputFileDataType->Text[1];
    TFileTP<double> CFGFile;
    TParameterTMI<double, double> TSUCFG;
    bool ExtrapolationFlag;
    AnsiString OutputfileName;
    AnsiString ErrorMsg = "";
    AnsiString WarningMsg = "";

    (Extrapolation_checkbox->Checked) ?
    ExtrapolationFlag = true : ExtrapolationFlag = false;

    if(InputFileDataType == 'f')
    {
        TFileTP<float> InputFile;
        TParameterTMI<float, float> InputParameter;
        TParameterTMI<float, float> OutputParameter;
        if(!InputFile.LoadFileTP(InputFileName, InputFileType, InputFileCountPar))
        {
            ErrorMsg = InputFile.GetError();
            if(Application->MessageBox(ErrorMsg.c_str(), "������!", MB_RETRYCANCEL | MB_ICONERROR) == 4)
            {
                ErrorMsg = "";
                return;
            }
            else
            {
                ErrorMsg = "";
                LE_CountPar->Clear();
                LE_ParNum->Clear();
                CB_InputFileType->ItemIndex = -1;
                CB_InputFileDataType->ItemIndex = -1;
                Extrapolation_checkbox->Checked = 0;
                return;
            }
        }
        else
        {
            WarningMsg = InputFile.GetWarning();
            if(WarningMsg != "")
            {
                if(Application->MessageBox(WarningMsg.c_str(), "��������!", MB_OKCANCEL | MB_ICONWARNING) == 2)
                {
                    WarningMsg = "";
                    LE_CountPar->Clear();
                    LE_ParNum->Clear();
                    CB_InputFileType->ItemIndex = -1;
                    CB_InputFileDataType->ItemIndex = -1;
                    Extrapolation_checkbox->Checked = 0;
                    return;
                }
            }
        }

        if(!CFGFile.LoadFileTP(CFGFileName, 116, 2))
        {
            ErrorMsg = CFGFile.GetError();
            if(Application->MessageBox(ErrorMsg.c_str(), "������!", MB_RETRYCANCEL | MB_ICONERROR) == 4)
            {
                ErrorMsg = "";
                return;
            }
            else
            {
                ErrorMsg = "";
                LE_CountPar->Clear();
                LE_ParNum->Clear();
                CB_InputFileType->ItemIndex = -1;
                CB_InputFileDataType->ItemIndex = -1;
                Extrapolation_checkbox->Checked = 0;
                return;
            }
        }
        else
        {
            WarningMsg = CFGFile.GetWarning();
            if(WarningMsg != "")
            {
                if(Application->MessageBox(WarningMsg.c_str(), "��������!", MB_OKCANCEL | MB_ICONWARNING) == 2)
                {
                    WarningMsg = "";
                    LE_CountPar->Clear();
                    LE_ParNum->Clear();
                    CB_InputFileType->ItemIndex = -1;
                    CB_InputFileDataType->ItemIndex = -1;
                    Extrapolation_checkbox->Checked = 0;
                    return;
                }
            }
        }

        InputParameter = InputFile.GetParameter(InputFileParNum);
        TSUCFG = CFGFile.GetParameter(1);
        OutputParameter = ScaleConversion(InputParameter, TSUCFG, ExtrapolationFlag, WarningMsg);
        if(WarningMsg != "")
        {
            Application->MessageBox(WarningMsg.c_str(), "��������!", MB_OK | MB_ICONWARNING);
            return;
        }
        else
        {
            OutputfileName = InputFileName + ".SC.P" + (AnsiString)InputFileParNum + ".f2";
            SaveToFile(OutputParameter, OutputfileName);
            Application->MessageBox("��������������� �������� ������� ��������.", "Scale Conversion", MB_OK | MB_ICONINFORMATION);
        }
    }
    else
    {
        TFileTP<double> InputFile;
        TParameterTMI<double, double> InputParameter;
        TParameterTMI<double, double> OutputParameter;
        if(!InputFile.LoadFileTP(InputFileName, InputFileType, InputFileCountPar))
        {
            ErrorMsg = InputFile.GetError();
            if(Application->MessageBox(ErrorMsg.c_str(), "������!", MB_RETRYCANCEL | MB_ICONERROR) == 4)
            {
                ErrorMsg = "";
                return;
            }
            else
            {
                ErrorMsg = "";
                LE_CountPar->Clear();
                LE_ParNum->Clear();
                CB_InputFileType->ItemIndex = -1;
                CB_InputFileDataType->ItemIndex = -1;
                Extrapolation_checkbox->Checked = 0;
                return;
            }
        }
        else
        {
            WarningMsg = InputFile.GetWarning();
            if(WarningMsg != "")
            {
                if(Application->MessageBox(WarningMsg.c_str(), "��������!", MB_OKCANCEL | MB_ICONWARNING) == 2)
                {
                    WarningMsg = "";
                    LE_CountPar->Clear();
                    LE_ParNum->Clear();
                    CB_InputFileType->ItemIndex = -1;
                    CB_InputFileDataType->ItemIndex = -1;
                    Extrapolation_checkbox->Checked = 0;
                    return;
                }
            }
        }

        if(!CFGFile.LoadFileTP(CFGFileName, 116, 2))
        {
            ErrorMsg = CFGFile.GetError();
            if(Application->MessageBox(ErrorMsg.c_str(), "������!", MB_RETRYCANCEL | MB_ICONERROR) == 4)
            {
                ErrorMsg = "";
                return;
            }
            else
            {
                ErrorMsg = "";
                LE_CountPar->Clear();
                LE_ParNum->Clear();
                CB_InputFileType->ItemIndex = -1;
                CB_InputFileDataType->ItemIndex = -1;
                Extrapolation_checkbox->Checked = 0;
                return;
            }
        }
        else
        {
            WarningMsg = CFGFile.GetWarning();
            if(WarningMsg != "")
            {
                if(Application->MessageBox(WarningMsg.c_str(), "��������!", MB_OKCANCEL | MB_ICONWARNING) == 2)
                {
                    WarningMsg = "";
                    LE_CountPar->Clear();
                    LE_ParNum->Clear();
                    CB_InputFileType->ItemIndex = -1;
                    CB_InputFileDataType->ItemIndex = -1;
                    Extrapolation_checkbox->Checked = 0;
                    return;
                }
            }
        }

        InputParameter = InputFile.GetParameter(InputFileParNum);
        TSUCFG = CFGFile.GetParameter(1);
        OutputParameter = ScaleConversion(InputParameter, TSUCFG, ExtrapolationFlag, WarningMsg);
        if(WarningMsg != "")
        {
            Application->MessageBox(WarningMsg.c_str(), "��������!", MB_OK | MB_ICONWARNING);
            return;
        }
        else
        {
            OutputfileName = InputFileName + ".SC.P" + (AnsiString)InputFileParNum + ".f2";
            SaveToFile(OutputParameter, OutputfileName);
            Application->MessageBox("��������������� �������� ������� ��������.", "Scale Conversion", MB_OK | MB_ICONINFORMATION);
        }
    }

}
//---------------------------------------------------------------------------



