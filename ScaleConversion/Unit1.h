//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <FileCtrl.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
    TGroupBox *InputFileGroup;
    TDriveComboBox *DriveComboBox;
    TFileListBox *InputFileListBox;
    TDirectoryListBox *DirectoryListBox;
    TGroupBox *CFGGroupBox;
    TFileListBox *CFGFileListBox;
    TButton *ScaleConv_BTN;
    TLabeledEdit *LE_CountPar;
    TLabeledEdit *LE_ParNum;
    TComboBox *CB_InputFileType;
    TLabel *Label_FileType;
    TComboBox *CB_InputFileDataType;
    TLabel *Label_DataType;
    TCheckBox *Extrapolation_checkbox;
    void __fastcall ScaleConv_BTNClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
