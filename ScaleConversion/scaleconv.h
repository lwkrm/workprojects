#ifndef scaleconvH
#define scaleconvH
#include <class_ParameterTMI.h>

template <class TypeData>
TParameterTMI<TypeData, TypeData> LineEq(TParameterTMI<TypeData, TypeData> Specification);


template <class TypeOfX, class TypeOfY, class TypeSpec>
TParameterTMI<TypeOfX, TypeOfY> ScaleConversion(    TParameterTMI<TypeOfX, TypeOfY> InputParameter,
                                                    TParameterTMI<TypeSpec, TypeSpec> Specification,
                                                    bool ExtrapolationFlag,
                                                    AnsiString &DiagnosticMsg);
//---------------------------------------------------------------------------
/*
������� ������� ������������� ������.
�������� Specification ���� TParameterTMI �������� ������� �����-�������������� ���.
�������� ������� - ������������ ������.
*/
template <class TypeData>
TParameterTMI<TypeData, TypeData> LineEq(TParameterTMI<TypeData, TypeData> Specification)
{
    TParameterTMI<TypeData, TypeData> OutputCoefficients;
    OutputCoefficients.CountPoint = Specification.CountPoint - 1;

    for(unsigned int i = 0; i < Specification.CountPoint - 1; i++)
    {
        //y=kx+b
        OutputCoefficients.Points[i].X = - (Specification.Points[i].Y - Specification.Points[i + 1].Y) / (Specification.Points[i + 1].X - Specification.Points[i].X ); //k
        OutputCoefficients.Points[i].Y = - (Specification.Points[i].X * Specification.Points[i + 1].Y - Specification.Points[i + 1].X * Specification.Points[i].Y) / (Specification.Points[i + 1].X - Specification.Points[i].X); //b
    }
    
    return OutputCoefficients;
}
//---------------------------------------------------------------------------
/*
������� ��������� ��������������.
��������� �������:
    InputParameter - ������� �������� (��������������� �����);
    ExtrapolationFlag - ���� ������ �������������;
    DiagnosticMsg - ��������������� ���������;
    Specification - ������� ������ �����-������������� ���.

    OutoutParameter - �������� �������� (���������� ��������).
    ����������� (����������� �����������) ������ � ������ ������������ ������� ����������.
    � ������ ���� ������� ��������� �� ������ ��������, �����������
    �������������� ��������������� ���������,
    � �������� ��������� - ������ ������ ������ TParametrTMI.
*/
template <class TypeOfX, class TypeOfY, class TypeSpec>
TParameterTMI<TypeOfX, TypeOfY> ScaleConversion(    TParameterTMI<TypeOfX, TypeOfY> InputParameter,
                                                    TParameterTMI<TypeSpec, TypeSpec> Specification,
                                                    bool ExtrapolationFlag,
                                                    AnsiString &DiagnosticMsg)
{
    TParameterTMI<TypeOfX, TypeOfY> OutputParameter;
    OutputParameter.CountPoint = InputParameter.CountPoint;

    TParameterTMI<TypeSpec, TypeSpec> LineCoeff = LineEq(Specification);

    bool BreakFlag = false;
    unsigned int i = 0;

    for(unsigned int j = 0; j <= Specification.CountPoint - 1; j++)
    {
        if(!BreakFlag)
        {
            while(i <= InputParameter.CountPoint - 1)
            {
                OutputParameter.Points[i].X = InputParameter.Points[i].X;

                if(InputParameter.Points[i].Y < Specification.Points[0].Y)
                {
                    if(ExtrapolationFlag == true)
                    {
                        OutputParameter.Points[i].Y = InputParameter.Points[i].Y * LineCoeff.Points[0].X + LineCoeff.Points[0].Y;
                        i++;
                        continue;
                    }
                    else
                    {
                        BreakFlag = true;
                        break;
                    }
                }

                if( InputParameter.Points[i].Y >= Specification.Points[j].Y
                    &&
                    InputParameter.Points[i].Y <= Specification.Points[j+1].Y)
                {
                    OutputParameter.Points[i].Y = InputParameter.Points[i].Y * LineCoeff.Points[j].X + LineCoeff.Points[j].Y;
                    i++;
                    continue;
                }

                if(InputParameter.Points[i].Y > Specification.Points[Specification.CountPoint - 1].Y)
                {
                    if(ExtrapolationFlag == true)
                    {
                        OutputParameter.Points[i].Y = InputParameter.Points[i].Y * LineCoeff.Points[LineCoeff.CountPoint - 1].X + LineCoeff.Points[LineCoeff.CountPoint - 1].Y;
                        i++;
                        continue;
                    }
                    else
                    {
                        BreakFlag = true;
                        break;
                    }
                }

                if(InputParameter.Points[i].Y > Specification.Points[j+1].Y)
                {
                        break;
                }
            }
        }
        else
        {
            OutputParameter.Clear();
            DiagnosticMsg = "������������� �������� �� ������������� �� �� ���.";;
            break;
        }
    }

    return OutputParameter;
}

#endif

